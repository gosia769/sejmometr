# README #

### What is this repository for? ###

Korzystanie z danych udostępnianych w webowym API.

System korzysta z dancyh udostępnianych w webowym API serwisu sejmometr https://mojepanstwo.pl/api/sejmometr
System, który na podstawie argumentów linii poleceń wyświetla następujące informacje (dla określonej kadencji sejmu):
-suma wydatków posła/posłanki o określonym imieniu i nazwisku
-wysokości wydatków na 'drobne naprawy i remonty biura poselskiego' określonego posła/posłanki
-średniej wartości sumy wydatków wszystkich posłów
-posła/posłanki, który wykonał najwięcej podróży zagranicznych
-posła/posłanki, który najdłużej przebywał za granicą
-posła/posłanki, który odbył najdroższą podróż zagraniczną
-listę wszystkich posłów, którzy odwiedzili Włochy