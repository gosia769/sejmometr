import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Gosia on 19/12/2016.
 */
public class MinisterDetailParserTest {

    Ministers ministers7 = new Ministers("https://api-v3.mojepanstwo.pl/dane/poslowie.json?conditions[poslowie.kadencja]=7");
    Ministers ministers8 = new Ministers("https://api-v3.mojepanstwo.pl/dane/poslowie.json?conditions[poslowie.kadencja]=8");

    public MinisterDetailParserTest() throws IOException {
    }


    @Test
    public void miniterTestTrip() throws Exception {
        assertEquals("Adam Szejnfeld", new Runner().parseArguments(new String[]{"kadencja", "7", "-trip"}));
    }

    @Test
    public void ministerTestAmount() throws Exception{
        assertEquals("Tadeusz Iwiński", ministers7.getMinisterTheLongestWereAbroat());
    }

    @Test
    public void miniterTestAbroad() throws Exception {
        assertEquals("Tadeusz Iwiński", ministers7.getMiniserTheBiggestAmountOfTravel());
    }
    @Test
    public void ministerTestAvg() throws IOException {
        assertEquals(272243.0108509064, ministers7.calculateavaragespendings(), 10);
    }
    @Test
    public void miniterTestItaly() throws Exception {
        assertEquals("Michał Jaros, Wojciech Penkalski, Ryszard Kalisz, Roman Jacek Kosecki, Grzegorz Raniewicz, Mariusz Grad, Andrzej Biernat, Jan Bury, Eugeniusz Tomasz Grzeszczak, Andrzej Gałażewski, Cezary Tomczyk, Artur Górczyński, Marek Matuszewski, Krzysztof Szczerski, Andrzej Czerwiński, Anna Fotyga, Ewa Kopacz, Anna Nemś, Stefan Niesiołowski, Cezary Grabarczyk, Stanisława Prządka, Antoni Mężydło, Stanisław Wziątek, Jan Dziedziczak, Adam Abramowicz, Tadeusz Iwiński, Tomasz Garbowski, Marek Rząsa, Joanna Fabisiak, Cezary Kucharski, Agnieszka Pomaska, Robert Tyszkiewicz, Beata Bublewicz, Jacek Falfus, Józef Zych, Jerzy Fedorowicz, Sławomir Neumann, Adam Rogacki, Jadwiga Zakrzewska, Rafał Grupiński, Grzegorz Schetyna, Piotr Tomański, Jakub Rutnicki, Andrzej Buła, Krystyna Skowrońska, Wojciech Ziemniak, Janusz Piechociński, Andrzej Gut-Mostowy, Bogusław Wontor, Ireneusz Raś, ".length(),
                new Runner().parseArguments(new String[]{"kadencja", "7", "-italy"}).length());
    }
    @Test
    public void miniterTestTravel2() throws Exception {
        assertEquals("Witold Waszczykowski", ministers8.getMinisterTheMostExpensiveTravel());
    }
    @Test
    public void miniterTestAmount2() throws Exception {
        assertEquals("Jan Dziedziczak", ministers8.getMiniserTheBiggestAmountOfTravel());
    }
    @Test
    public void miniterTestAbroat2() throws Exception {
        assertEquals("Jan Dziedziczak", ministers8.getMinisterTheLongestWereAbroat());
    }

    @Test
    public void ministerTestIatly8 () throws IOException {
        assertEquals("Antoni Mężydło, Adam Abramowicz, Roman Kosecki, Agnieszka Pomaska, Robert Tyszkiewicz, Anna Nemś, Grzegorz Raniewicz, Ewa Kopacz, Joanna Fabisiak, Andrzej Czerwiński, Cezary Tomczyk, Ireneusz Raś, Grzegorz Schetyna, Jacek Falfus, Cezary Grabarczyk, Marek Rząsa, Sławomir Neumann, Jan Dziedziczak, Marek Matuszewski, Jakub Rutnicki, Stefan Niesiołowski, Wojciech Ziemniak, Rafał Grupiński, Michał Jaros, Krystyna Skowrońska,".length(),
                     new Runner().parseArguments(new String[] {"kadencja",  "8",  "-italy"}).length());

    }

    @Test
    public void ministerTestAvg2() throws IOException {
        assertEquals(144882.96565824468 , ministers8.calculateavaragespendings(), 10);
    }



}
//assert
//277061,64  id 174