import java.io.IOException;
import java.net.UnknownHostException;

/**
 * Created by Gosia on 10/01/2017.
 */
public class App {
    public static void main(String[] args) throws IOException {
        try {
//            System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "8");
            Runner runner = new Runner();
            System.out.println(runner.parseArguments(args));
        } catch (UnknownHostException e) {
            System.out.println("Nie ma polaczenia z internetem");
        } catch (IllegalArgumentException e) {
            if (e.getMessage() == null) {
                System.out.println("Podano zle argumenty");
            } else System.out.println("Podano zle argumenty " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

}
