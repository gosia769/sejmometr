import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by Gosia on 16/12/2016.
 */
// Ta klasa sluzy do przetowrzenia glownej listy poslow, wynikiem jej pracy jej hashmapa,
// glowna metoda to parseministerlist, inne sluza tylko do pomocy, mapa misisters jest
//    mapa tworzona podczas pracy obiektu tej klasy i zwracana na koniec
class MinisterListParser {
    private HashMap<String, Integer> hashministers = new HashMap<>();

    HashMap<String, Integer> parseministerlist(String url) throws IOException {
        String nextpage = url;
        while (!nextpage.equals(" ")) {
            nextpage = parseministerpage(nextpage);
        }
        return hashministers;
    }

    private String parseministerpage(String url) throws IOException {
        try (InputStream in = new URL(url).openStream()) {

            String input = IOUtils.toString(in, "UTF-8");
            JSONObject object = new JSONObject(input);

            for (Object i : object.getJSONArray("Dataobject")) {
                parse((JSONObject) i);
            }
            if (object.getJSONObject("Links").has("next")) {
                return object.getJSONObject("Links").getString("next");
            } else
                return " ";
        }
    }

    private void parse(JSONObject obj) {
        String id = obj.getString("id");
        String nazwa = obj.getJSONObject("data").getString("poslowie.nazwa");
        hashministers.put(nazwa, Integer.valueOf(id));
    }
}
