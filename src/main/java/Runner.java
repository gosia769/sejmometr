import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Gosia on 09/01/2017.
 */
class Runner {
    private static String url = "https://api-v3.mojepanstwo.pl/dane/poslowie.json";

    private Minister checkOneMinister(String name) throws IOException{
           MinisterListParser ministerListParser = new MinisterListParser();
           HashMap<String, Integer> hashministers = ministerListParser.parseministerlist(url);
           if(hashministers.containsKey(name)) {
               int idPosla = hashministers.get(name);
               return new Minister(idPosla, name, Ministers.parseMinisterDetail(name, idPosla));
           }else throw new IllegalArgumentException("Podano niepoprawne imie i nazwisko posla");
    }

    private Ministers checkAllMinisters() throws IOException {
       return new Ministers(url);
    }

    //parsuje linie komen
    // kadencja 8 -s "Imie Nazwisko"
    // kadencja 7 -avarage

    String parseArguments(String[] arguments) throws IllegalArgumentException, IOException {
        if (arguments.length< 3 || arguments.length > 5 || arguments.length == 4 ||
               !arguments[0].equals("kadencja") ||
                (!arguments[1].equals("7") && !arguments[1].equals("8"))) {
            throw new IllegalArgumentException();
        }
        if (arguments[1].equals("7")) {
            url = "https://api-v3.mojepanstwo.pl/dane/poslowie.json?conditions[poslowie.kadencja]=7";
        } else if (arguments[1].equals("8")) {
            url = "https://api-v3.mojepanstwo.pl/dane/poslowie.json?conditions[poslowie.kadencja]=8";
        }
        if (arguments.length == 5) {
            switch (arguments[2]) {
                case "-s":
                    String name = arguments[3] + " " + arguments[4];
                    return checkOneMinister(name).countspendings().toString();

                case "-sm":
                    name = arguments[4] + " " + arguments[5];
                    return checkOneMinister(name).countminorspendings().toString();
                default:
                    throw new IllegalArgumentException();
            }
        } else if (arguments.length == 3) {
            switch (arguments[2]) {
                case "-avg":
                    return checkAllMinisters().calculateavaragespendings().toString();
                case "-amount":
                    return checkAllMinisters().getMiniserTheBiggestAmountOfTravel();
                case "-abroad":
                    return checkAllMinisters().getMinisterTheLongestWereAbroat();
                case "-trip":
                    return checkAllMinisters().getMinisterTheMostExpensiveTravel();
                case "-italy":
                    return checkAllMinisters().getMinistersWhoWereInItaly().toString();
                case "-all":
                    Ministers ministers = checkAllMinisters();
                    return "Evarage spendings: " + ministers.calculateavaragespendings().toString() + "\n" +
                            "The biggest amount of travel: " + ministers.getMiniserTheBiggestAmountOfTravel() + "\n" +
                            "The longest were abroat: " + ministers.getMinisterTheLongestWereAbroat() + "" + "\n" +
                            "MInister who went on the most expensive travel: " + ministers.getMinisterTheMostExpensiveTravel() + "\n" +
                            "Ministers who were in Italy: " + ministers.getMinistersWhoWereInItaly();
                default:
                    throw new IllegalArgumentException();
            }
        }return "";
    }
}
// -s "imie nazwisko"  - suma wydatkow
//-sm "imie nazwisko"  - suma maych wydatkow
// -avg srednia suma wydatow wszytkich poslow
// -amount najwiecej podrozy zagranicznych
// -abroat  najdluzej przebywal za granica
// -trip  -  najdrozsza podroz zagraniczna
// -italy  - poslowie  ktorzy byli we wloszech






