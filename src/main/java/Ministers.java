import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Gosia on 16/12/2016.
 */
//tej klasy uzywam tylko wtedy gdy potrzebuje informacji o wszytkich poslach
//
class Ministers {

    private HashMap<String, Integer> hashministers = new HashMap<>();
    private List<Minister> ministers = Collections.synchronizedList(new ArrayList<>());

    Ministers(String url) throws IOException {
        loadHashministers(url);
        createListMinisters();
    }

    private void loadHashministers(String url) throws IOException {
        MinisterListParser ministerListParser = new MinisterListParser();
        hashministers = ministerListParser.parseministerlist(url);
    }

    private void createListMinisters() throws IOException {
        hashministers.entrySet().parallelStream().forEach(p -> {
            try {
                addMinister(p.getValue(), p.getKey(), parseMinisterDetail(p.getKey(), p.getValue()));
            } catch (IOException e) {
                System.out.println("nie udalo sie stworzyc listy poslow");
            }
        });
//
//                   for(Map.Entry<String, Integer> entry : hashministers.entrySet()) {
//                       String name = entry.getKey();
//                       Integer idPosla = entry.getValue();
//                       addMinister(idPosla, name, parseministerdetail(name, idPosla));
//                   }
    }

    static JSONObject parseMinisterDetail(String name, Integer idPosla) throws IOException {
        String url = "https://api-v3.mojepanstwo.pl/dane/poslowie/" + idPosla + ".json?layers[]=wyjazdy&layers[]=wydatki";
        return parseMinisterPage(url);
    }

    private static JSONObject parseMinisterPage(String url) throws IOException {
        try (InputStream in = new URL(url).openStream()) {
            String input = IOUtils.toString(in, "UTF-8");
            return new JSONObject(input);
        }
    }

    private boolean addMinister(Integer idPosla, String name, JSONObject jsonObject) {
        synchronized (ministers) {
            return ministers.add(new Minister(idPosla, name, jsonObject));
        }
    }

    String getMinisterTheMostExpensiveTravel() {
        Double cost = 0d;
        String name = "";
        for (Minister i : ministers) {
            if (i.countthemostexpensivetravel() > cost) {
                name = i.name;
                cost = i.countthemostexpensivetravel();
            }
        }
        return name;
    }

    String getMiniserTheBiggestAmountOfTravel() {
        int amount = 0;
        String name = "";
        for (Minister i : ministers) {
            if (i.countamountofdaysabroat() > amount) {
                name = i.name;
                amount = i.countamountoftravels();
            }
        }
        return name;
    }

    String getMinisterTheLongestWereAbroat() {
        Integer length = 0;
        String name = "";
        for (Minister i : ministers) {
            if (i.countamountofdaysabroat() > length) {
                name = i.name;
                length = i.countamountofdaysabroat();
            }
        }
        return name;
    }

    List<String> getMinistersWhoWereInItaly() {
        return ministers.stream().filter(Minister::checkifwereinitaly).map(i -> i.name).collect(Collectors.toList());
    }

    Double calculateavaragespendings() {
        int amountofministers = 0;
        Double amountofspendings = 0d;
        for (Minister i : ministers) {
            amountofspendings = amountofspendings + i.countspendings();
            amountofministers++;
        }
        return amountofspendings / ((double) amountofministers);
    }
}


