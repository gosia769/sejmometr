import org.json.JSONObject;

/**
 * Created by Gosia on 16/12/2016.
 */
class Minister {
    private Integer id;
    String name;
    private JSONObject jsonObject;
//metody mogbyc statyczne uzywam ich tylko raz!!

    Minister(Integer id, String name, JSONObject jsonObject) {
        this.id = id;
        this.jsonObject = jsonObject;
        this.name = name;
    }

    Double countspendings() {
        Double spendings = 0d;
        for (Object i : jsonObject.getJSONObject("layers").getJSONObject("wydatki").getJSONArray("roczniki")) {
            JSONObject year = (JSONObject) i;
            for (Object j : year.getJSONArray("pola")) {
                Double cost = Double.parseDouble((String) j);
                spendings = spendings + cost;
            }
        }
        return spendings;
    }

    Double countminorspendings() {
        Double minorspendings = 0d;
        for (Object i : jsonObject.getJSONObject("layers").getJSONObject("wydatki").getJSONArray("roczniki")) {
            JSONObject year = (JSONObject) i;
            Double cost = year.getJSONArray("pola").getDouble(12);
            minorspendings = minorspendings + cost;
        }
        return minorspendings;
    }

    int countamountofdaysabroat() {
        int amountOfDaysAbroat = 0;
        if (jsonObject.getJSONObject("layers").optJSONArray("wyjazdy") == null) {
            return 0;
        } else {
            for (Object i : jsonObject.getJSONObject("layers").getJSONArray("wyjazdy")) {
                JSONObject travel = (JSONObject) i;
                Integer amountOfDays = travel.getInt("liczba_dni");
                amountOfDaysAbroat = amountOfDaysAbroat + amountOfDays;
            }
        }
        return amountOfDaysAbroat;
    }


    int countamountoftravels() {
        if (jsonObject.getJSONObject("layers").optJSONArray("wyjazdy") == null) return 0;
        return jsonObject.getJSONObject("layers").getJSONArray("wyjazdy").length();
    }


    Double countthemostexpensivetravel() {
        Double themostexpensivetravel = 0d;
        if (jsonObject.getJSONObject("layers").optJSONArray("wyjazdy") == null) return 0d;
        else {
            for (Object i : jsonObject.getJSONObject("layers").getJSONArray("wyjazdy")) {
                JSONObject travel = (JSONObject) i;
                Double expense = travel.getDouble("koszt_suma");
                if (themostexpensivetravel < expense) {
                    themostexpensivetravel = expense;
                }
            }
            return themostexpensivetravel;
        }
    }

    boolean checkifwereinitaly() {
        if (jsonObject.getJSONObject("layers").optJSONArray("wyjazdy") == null) return false;
        else {
            for (Object i : jsonObject.getJSONObject("layers").getJSONArray("wyjazdy")) {
                JSONObject travel = (JSONObject) i;
                String country = travel.getString("kraj");
                if (country.equals("Włochy")) {
                    return true;
                }
            }
        }
        return false;
    }
}


